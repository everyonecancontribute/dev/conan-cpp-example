#include <iostream>
#include <boost/any.hpp>
#include <string>

int main(int argc, char *argv[])
{
    std::cout << "hello boost:-D\n";

    boost::any variable(std::string("Hello world!"));
    std::string s1 = boost::any_cast<std::string>(variable);
    
    std::cout << "any is " << s1 << '\n';

    return 0;
}
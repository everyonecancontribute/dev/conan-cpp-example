project(PackageTest CXX)
cmake_minimum_required(VERSION 2.8.12)

set(CMAKE_CXX_STANDARD 11)
if(NOT WIN32)
    set(CMAKE_CXX_FLAGS "-fPIC ${CMAKE_CXX_FLAGS}")
endif()

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup(TARGETS)

set(CPP_FILES
    example.cpp
)

add_executable(example
    ${CPP_FILES}
)
#target_link_libraries(example CONAN_PKG::example)

import os
from conans import ConanFile, CMake, tools

channel = os.getenv("CONAN_CHANNEL", "stable")
username = os.getenv("CONAN_USERNAME", "zkw")

class ExampleTest(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    #requires = "LDS/3.0.0@%s/%s" % (username, channel)
    generators = "cmake"

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_dir=self.source_folder, build_dir="./")
        cmake.build()

    def imports(self):
        self.copy("*.dll", dst="bin", src="bin")
        self.copy("*.dylib*", dst="bin", src="lib")
        self.copy("*.so*", dst="lib", src="lib")

    def test(self):
        with tools.chdir("bin"):
            self.run(".%sexample" % os.sep)
        self.run("heyconsole")
